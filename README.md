# Game: La Leyenda del Zafiro Azul :gem:

## Detalles del juego:
### Géneros: 
- Metroidvania

### Historia/Narrativa:
- Enlace: https://docs.google.com/document/d/1BZj5AP-vdMHXddft0yVjPvQpP-YlAbeKDIc9B3FsqkU/edit

### MDA: 
- Enlace: https://docs.google.com/document/d/1FMTGB2qww2xEuyNj6Wpyq32MrxOGueGc/edit

## :busts_in_silhouette: :computer: Equipo de desarrollo:
- :man: Eduardo Angulo
- :man: Juan Pablo Duque
- :woman: Yudy Herrera
- :man: Victor Paternina
- :man: Alberto Pumarejo

## :fast_forward: Avances del proyecto:

### **Niveles**
#### :hammer: En proceso: 
- 7 niveles (0-6) implementados con la lógica diseñada previamente para el avance del jugador (Región 1).
- Cambio entre niveles.
- Reinicio de nivel al morir el personaje.

#### :wrench: Por arreglar:

### **Jugador**
#### :hammer: En proceso: 
- Personaje con animaciones de correr.  
- Personaje con animación de caminar.  
- Personaje con animación de atacar.  
#### :wrench: Por arreglar:
- Personaje salta pero sin la animación correspondiente. 
### **Mapa**
#### :hammer: En proceso: 
- Interfaz del mapa accesible.  
- Identifica el nivel donde estás parpadeando esa zona.  
- Solo se activan los niveles si has estado en ellos.  
#### :wrench: Por arreglar:

### **Enemigos**
#### :hammer: En proceso:
- Enemigo persigue al jugador.
#### :wrench: Por arreglar:
- Enemigo no se mantiene en el eje de juego del jugador.

### **Sonidos**
#### :hammer: En proceso: 
- Sonido de fondo para la cueva (niveles 0-5).  
- Sonido de fondo para el bosque (nivel 6).  
- Sonidos de pasos en el jugador.
#### :wrench: Por arreglar: 
- El sonido de pasos casi nunca funciona.
- 

### **UI/HUD**
#### :hammer: En proceso: 
- Interfaz de barra de vida (HP) del personaje principal.
- Interfaz de mapa funcional.
- Interfaz mensaje muerte funcional.
- Interfaz Fade(In-Out) funcional.

### **Eventos del personaje principal**
#### :hammer: Implementado: 
- Muere al perder toda la vida.

#### :wrench: Por arreglar: 


## :fast_forward: :fast_forward: Futuros avances:


